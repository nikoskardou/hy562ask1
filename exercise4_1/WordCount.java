package relFreqStripes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.StringUtils;
import org.apache.log4j.Logger;


public class WordCount extends Configured implements Tool {

  private static final Logger LOG = Logger.getLogger(WordCount.class);

  public static void main(String[] args) throws Exception {
    int res = ToolRunner.run(new WordCount(), args);
    System.exit(res);
  }

  
  public int run(String[] args) throws Exception {
    Job job = Job.getInstance(getConf(), "wordcount");
    for (int i = 0; i < args.length; i += 1) {
      if ("-skip".equals(args[i])) {
    	job.getConfiguration().setBoolean("wordcount.skip.patterns", true);
        i += 1;
        job.addCacheFile(new Path(args[i]).toUri());
        // this demonstrates logging
        LOG.info("Added file to the distributed cache: " + args[i]);
      }
    }
    
    job.addCacheFile(new Path("/home/cloudera/workspace/RelFreqStripes/stopwords.csv").toUri());

    
    job.setJarByClass(this.getClass());
    // Use TextInputFormat, the default unless job.setInputFormatClass is used
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    job.setMapperClass(Mapper1.class);
    job.setReducerClass(Reducer1.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(Text.class);
    return job.waitForCompletion(true) ? 0 : 1;
  }

  
  public static class Mapper1 extends Mapper<LongWritable, Text, Text, Text> {
    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();
    private boolean caseSensitive = false;
    private long numRecords = 0;
    private String input;
    private Set<String> patternsToSkip = new HashSet<String>();
    private static final Pattern WORD_BOUNDARY = Pattern.compile("\\s*\\b\\s*");
    private static final String FILTER = "[a-zA-Z]+";

    protected void setup(Mapper.Context context)
        throws IOException,InterruptedException {
	      Configuration config = context.getConfiguration();
	      this.caseSensitive = config.getBoolean("wordcount.case.sensitive", false);
	      URI[] localPaths = context.getCacheFiles();
	      parseSkipFile(localPaths[0]);
    }

    
    private void parseSkipFile(URI patternsURI) {
      LOG.info("Added file to the distributed cache: " + patternsURI);
      try {
        BufferedReader fis = new BufferedReader(new FileReader(new File(patternsURI.getPath()).getName()));
        String pattern;
        while ((pattern = fis.readLine()) != null) {
          patternsToSkip.add(pattern);
        }
      } catch (IOException ioe) {
        System.err.println("Caught exception while parsing the cached file '"
            + patternsURI + "' : " + StringUtils.stringifyException(ioe));
      }
    }

    
    public void map(LongWritable offset, Text lineText, Context context)
        throws IOException, InterruptedException {
      String line = lineText.toString();      
      
      if (!caseSensitive) {
        line = line.toLowerCase();
      }
      
      for (String word_1 : WORD_BOUNDARY.split(line)) {
          
    	  HashMap<String, Integer> stripe = new HashMap<String, Integer>();
    	  if (word_1.isEmpty() || patternsToSkip.contains(word_1)|| !word_1.matches(FILTER)) {
			  continue;
		  }
    	  for (String word_2 : WORD_BOUNDARY.split(line)) {
    		//Check if word is present in the HashMap ,if File name is not present then add the word to the HashMap and increment the counter by one , This condition will be satisfied on first occurrence of that word
    		  if (word_2.isEmpty() || patternsToSkip.contains(word_2) || !word_2.matches(FILTER)) {
    			  continue;
    		  }
    		  if(word_1.equals(word_2)){
    			  continue;
    		  }
    		  if(stripe!=null){
  					//Check if word is present in the HashMap ,if word is not present then add the word to the HashMap and increment the counter by one , This condition will be satisfied on first occurrence of that word
    			  	if(stripe.get(word_2)!=null){
    			  		int count=(int)stripe.get(word_2);
    			  		stripe.put(word_2, ++count);
    			  	}else{
    			  	//Else part will execute if word is already added then just increase the count for that word which is stored as key in the hash map
    			  		stripe.put(word_2, 1);
    			  	}
    		  }    		  
    	  }
    	  
    	  String s = "";
    	  for(String key : stripe.keySet() ){
    		  s += key + ":" + stripe.get(key).toString() + ",";
    	  }
  
    	  context.write(new Text(word_1), new Text(s));
    	  
        }             
    }
  }
  
  
  public static class Reducer1 extends Reducer<Text, Text, Text, Text> {
	  TreeSet<Pair> set = new TreeSet<Pair>();
      
		@Override
	    public void reduce(Text word, Iterable<Text> stripes, Context context)
	        throws IOException, InterruptedException {
			
			HashMap<String, Integer> TotalStripes = new HashMap<String, Integer>();
			double totalCount = 0;
			
			for(Text stripe : stripes ){
				
				String[] values = stripe.toString().split(",");
				for(String value : values){
					String[] split = value.toString().split(":");
					if(split.length  == 2){
						String word_2 = split[0];
						String counter = split[1];
						
						if(TotalStripes!=null){
		  					//Check if word is present in the HashMap ,if word is not present then add the word to the HashMap and increment the counter by one , This condition will be satisfied on first occurrence of that word
		    			  	if(TotalStripes.get(word_2)!=null){
		    			  		int count=(int)TotalStripes.get(word_2);
		    			  		TotalStripes.put(word_2, count + Integer.parseInt(counter));
		    			  	}else{
		    			  		//Else part will execute if word is already added then just increase the count for that word which is stored as key in the hash map
		    			  		TotalStripes.put(word_2, Integer.parseInt(counter));	    			  			    			  		
		    			  	}
						}
						totalCount += Integer.parseInt(counter);
					}
				}								
			}
			
			for(String key: TotalStripes.keySet()){
				set.add(new Pair( (TotalStripes.get(key)/totalCount), word.toString(), key));
				if(set.size() > 100){
					set.pollFirst();
				}
			}
			
	    }
		
		
		protected void cleanup(Context context) throws IOException,InterruptedException {
            while (!set.isEmpty()) {
                Pair pair = (Pair) set.last();
                set.pollLast();
                context.write(new Text(pair.word_1 + " " + pair.word_2), new Text(String.valueOf(pair.relativeFrequency)));
            }
        }
		
		
		class Pair implements Comparable<Pair> {
            double relativeFrequency;
            String word_1;
            String word_2;

            Pair(double relativeFrequency, String word_1, String word_2) {
                this.relativeFrequency = relativeFrequency;
                this.word_1 = word_1;
                this.word_2 = word_2;
            }

            @Override
            public int compareTo(Pair pair) {
                if (this.relativeFrequency >= pair.relativeFrequency) {
                    return 1;
                } else {
                    return -1;
                }
            }
        }
		
	  }
	
  
}

