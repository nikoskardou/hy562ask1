/*
 * HY562 - Assignment 1
 * 
 * Nikos Kardoulakis
 * 
 * kardoulakis@csd.uoc.gr
 * 
 */

package exercise1;

import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.RawComparator;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.util.StringUtils;
import org.apache.log4j.Logger;


public class WordCount extends Configured implements Tool {
	private static final Logger LOG = Logger.getLogger(WordCount.class);
	public static void main(String[] args) throws Exception {
		long startTime = System.currentTimeMillis();
		int res = ToolRunner.run(new WordCount(), args);
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		long total = (long)totalTime / 1000;
		System.out.println("total time = " + totalTime);
		System.out.println("total time sec= " + total);
		System.exit(res);
	}

	
	public static class DescendingIntWritableComparable extends IntWritable {
	    /** A decreasing Comparator optimized for IntWritable. */ 
	    public static class DecreasingComparator extends Comparator {
	        public int compare(WritableComparable a, WritableComparable b) {
	            return -super.compare(a, b);
	        }
	        public int compare(byte[] b1, int s1, int l1, byte[] b2, int s2, int l2) {
	            return -super.compare(b1, s1, l1, b2, s2, l2);
	        }
	    }
	}
	
	public int run(String[] args) throws Exception {
		
		Path output = new Path(args[1]);
		
		Configuration conf = new Configuration(); 
		conf.setBoolean("mapred.compress.map.output", true); 
		conf.set("mapred.map.output.compression.codec","org.apache.hadoop.io.compress.DefaultCodec");
		
		/* set Job 1. It implements map reduce */
		Job job_1 = Job.getInstance(conf, "wordcount");
		job_1.setJarByClass(this.getClass());		
		job_1.setMapperClass(TokenMapper.class);
		job_1.setCombinerClass(SumReducer.class);
		job_1.setReducerClass(SumReducer.class);
		job_1.setNumReduceTasks(25);
		job_1.setOutputKeyClass(Text.class);
		job_1.setOutputValueClass(IntWritable.class);
		FileInputFormat.addInputPath(job_1, new Path(args[0]));
		FileOutputFormat.setOutputPath(job_1, new Path(output, "out1"));
		if(!job_1.waitForCompletion(true)){
			System.exit(1);
		}
		//return job_1.waitForCompletion(true) ? 0 : 1;
		
		/* Set Job_2. It swaps [key,value] pairs from Job_1 and sorts */
		Job job_2 = Job.getInstance(conf, "swap");
		job_2.setJarByClass(this.getClass());
		job_2.setMapperClass(SwapMapper.class);
		job_2.setReducerClass(SwapReducer.class);
		job_2.setNumReduceTasks(25);
		job_2.setSortComparatorClass(DescendingIntWritableComparable.DecreasingComparator.class);
		job_2.setOutputKeyClass(IntWritable.class);
		job_2.setOutputValueClass(Text.class);
		job_2.setInputFormatClass(KeyValueTextInputFormat.class);
		FileInputFormat.addInputPath(job_2, new Path(output, "out1"));
		FileOutputFormat.setOutputPath(job_2, new Path(output, "out2"));		
		return job_2.waitForCompletion(true) ? 0 : 1;
	}
	
	
	/* Mapper and Reducer Classes for Job #1 */
	public static class TokenMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
		private final static IntWritable one = new IntWritable(1);
		private Text word = new Text();
		//private boolean caseSensitive = false;
		private static final Pattern WORD_BOUNDARY = Pattern.compile("\\s*\\b\\s*");
		private static final String FILTER = "[a-zA-Z]+";
    
    
	    public void map(LongWritable offset, Text lineText, Context context)
	    	throws IOException, InterruptedException {
	    	String line = lineText.toString();	     
		    Text currentWord = new Text();
		    for (String word : WORD_BOUNDARY.split(line)) {
		    	if (word.isEmpty() || !word.matches(FILTER)) {
		    		continue;
		    	}
		    	word = word.toLowerCase();
		    	currentWord = new Text(word);
		    	context.write(currentWord,one);
		    }         
	    }
	}

	public static class SumReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
		@Override
		public void reduce(Text word, Iterable<IntWritable> counts, Context context)
				throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable count : counts) {
				sum += count.get();
			}
			context.write(word, new IntWritable(sum));
		}
	}
	
	
	/* Mapper Class for Job #2 */
	public static class SwapMapper extends Mapper<Text, Text, IntWritable, Text> {

	    public void map(Text word, Text count, Context context)
	    	throws IOException, InterruptedException {
	    		if(Integer.parseInt(count.toString()) > 4000){
	    			context.write(new IntWritable(Integer.parseInt(count.toString())),word); 
	    		}
	    }
	}
	
	public static class SwapReducer extends Reducer<IntWritable, Text, Text, IntWritable> {
		@Override
		public void reduce(IntWritable count, Iterable<Text> words, Context context)
				throws IOException, InterruptedException {

				for (Text word : words) {
					context.write(word, count);
				}
		}
	}
	
	
}