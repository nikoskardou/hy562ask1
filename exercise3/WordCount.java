package exercise3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.StringUtils;
import org.apache.log4j.Logger;


public class WordCount extends Configured implements Tool {

  private static final Logger LOG = Logger.getLogger(WordCount.class);
  
  public enum MY_COUNTER {
	  WORDS_IN_ONE_DOC
	  };

  public static void main(String[] args) throws Exception {
    int res = ToolRunner.run(new WordCount(), args);
    System.exit(res);
  }

  public int run(String[] args) throws Exception {
    Job job_1 = Job.getInstance(getConf(), "wordcount");
    for (int i = 0; i < args.length; i += 1) {
      if ("-skip".equals(args[i])) {
        job_1.getConfiguration().setBoolean("wordcount.skip.patterns", true);
        i += 1;
        job_1.addCacheFile(new Path(args[i]).toUri());
        // this demonstrates logging
        LOG.info("Added file to the distributed cache: " + args[i]);
      }
    }
    
    job_1.addCacheFile(new Path("/home/cloudera/workspace/WordCount_2/stopwords.csv").toUri());
    
    Path output = new Path(args[1]);
    
    job_1.setJarByClass(this.getClass());
    // Use TextInputFormat, the default unless job.setInputFormatClass is used
    FileInputFormat.addInputPath(job_1, new Path(args[0]));
    FileOutputFormat.setOutputPath(job_1, new Path(output, "out1"));
    job_1.setMapperClass(Mapper1.class);
    job_1.setCombinerClass(Reducer1.class);
    job_1.setReducerClass(Reducer1.class);
    job_1.setOutputKeyClass(Text.class);
    job_1.setOutputValueClass(Text.class);
    if(!job_1.waitForCompletion(true)){
		System.exit(1);
	}
    
    /* Set Job_2. It swaps [key,value] pairs from Job_1 and sorts */
	Job job_2 = Job.getInstance(getConf(), "index");
	job_2.setJarByClass(this.getClass());
	job_2.setMapperClass(Mapper2.class);
    job_2.setReducerClass(Reducer2.class);
	job_2.setOutputKeyClass(Text.class);
	job_2.setOutputValueClass(Text.class);
	job_2.setInputFormatClass(KeyValueTextInputFormat.class);
	FileInputFormat.addInputPath(job_2, new Path(output, "out1"));
	FileOutputFormat.setOutputPath(job_2, new Path(output, "out2"));		
	return job_2.waitForCompletion(true) ? 0 : 1;

  }

  public static class Mapper1 extends Mapper<LongWritable, Text, Text, Text> {
    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();
    private boolean caseSensitive = false;
    private long numRecords = 0;
    private String input;
    private Set<String> patternsToSkip = new HashSet<String>();
    private static final Pattern WORD_BOUNDARY = Pattern.compile("\\s*\\b\\s*");
    private static final String FILTER = "[a-zA-Z]+";

    protected void setup(Mapper.Context context)
        throws IOException,
        InterruptedException {
      Configuration config = context.getConfiguration();
      this.caseSensitive = config.getBoolean("wordcount.case.sensitive", false);
        URI[] localPaths = context.getCacheFiles();
        parseSkipFile(localPaths[0]);
    }

    private void parseSkipFile(URI patternsURI) {
      LOG.info("Added file to the distributed cache: " + patternsURI);
      try {
        BufferedReader fis = new BufferedReader(new FileReader(new File(patternsURI.getPath()).getName()));
        String pattern;
        while ((pattern = fis.readLine()) != null) {
          patternsToSkip.add(pattern);
        }
      } catch (IOException ioe) {
        System.err.println("Caught exception while parsing the cached file '"
            + patternsURI + "' : " + StringUtils.stringifyException(ioe));
      }
    }

    public void map(LongWritable offset, Text lineText, Context context)
        throws IOException, InterruptedException {
      String line = lineText.toString();
      String fileName = ((FileSplit)context.getInputSplit()).getPath().getName();
      if (!caseSensitive) {
        line = line.toLowerCase();
      }
      Text currentWord = new Text();
      for (String word : WORD_BOUNDARY.split(line)) {
        if (word.isEmpty() || patternsToSkip.contains(word) || !word.matches(FILTER)) {
            continue;
        }
            currentWord = new Text(word);
            context.write(currentWord,new Text(fileName));
        }             
    }
  }
  
  
  public static class Reducer1 extends Reducer<Text, Text, Text, Text> {
      
		@Override
	    public void reduce(Text word, Iterable<Text> files, Context context)
	        throws IOException, InterruptedException {
			
			StringBuilder str = new StringBuilder();
			for(Text x : files){
				if(str.length() > 0){
					str.append(" ");
				}
				str.append(x.toString());
			}
			
			context.write(word, new Text(str.toString()));
	    }
	  }
	
	
	/* Mapper Class for Job #2 */
	public static class Mapper2 extends Mapper<Text, Text, Text, Text> {

	    public void map(Text word, Text files, Context context)
	    	throws IOException, InterruptedException {
	    		
	    	String[] file_names = files.toString().split(" ");
	    	
	    	//Declare the Hash Map to store File name as key to compute and store number of times the filename is occurred for as value
			HashMap<String, Integer> m=new HashMap<String, Integer>();
			int count=0;
			for(String t:file_names){
				//Check if file name is present in the HashMap ,if File name is not present then add the Filename to the HashMap and increment the counter by one , This condition will be satisfied on first occurrence of that word
				if(m!=null &&m.get(t)!=null){
					count=(int)m.get(t);
					m.put(t, ++count);
				}else{
					//Else part will execute if file name is already added then just increase the count for that file name which is stored as key in the hash map
					m.put(t, 1);
				}
			}
			// Emit word and [file1→count of the word1 in file1 , file2→count of the word1 in file2 ………] as output
			
			String s = "";
			for(String key : m.keySet() ){
				s += key + " #" + m.get(key).toString() + " ";
			}
			
			
			context.write(word, new Text(s));
			//context.write(word, new Text(m.toString()));
	    }
	}
  

  public static class Reducer2 extends Reducer<Text, Text, Text, Text> {
      
	static int id = 1;  
	@Override
    public void reduce(Text word, Iterable<Text> entries, Context context)
        throws IOException, InterruptedException {
		
		for(Text entry : entries){
			context.write(new Text(id + " " + word.toString()), entry);
		}
		id++;
		
		
    }
  }
  
}

